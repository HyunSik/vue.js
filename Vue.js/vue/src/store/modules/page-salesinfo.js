import Constant from '../../Constant'
import Conf from '../../Config'
import axios from 'axios'

const state = {}

// getters
const getters = {}

// mutations
const mutations = {}

// actions
const actions = {}

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
}