import Vue from 'vue';
import Vuex from 'vuex';
import page_customer from './modules/page-customer'

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {
        m1: page_customer
    }
})

export default store;