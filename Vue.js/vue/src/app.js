import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import store from './store/store.js'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import Vuelidate from 'vuelidate'
import { Pagination } from 'bootstrap-vue/es/components';
import Conf from './Config'

Vue.use(Pagination);
Vue.use(Vuelidate)
Vue.use(BootstrapVue);

Vue.prototype.$Conf = Conf; // 모든 컴포넌트에서 this.$Conf로 접근 가능
Vue.prototype.$axios = axios; // 모듈에서도 전역으로 사용하게 제작해야 함... ㅠ.ㅠ

new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App)
})