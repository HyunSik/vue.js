﻿using System.Collections.Generic;
using Core.Models.Abstract;
using Core.Models.Entities;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private ICustomerRepository _repository;

        public CustomerController(ICustomerRepository repository)
        {
            _repository = repository;
        }

        // GET api/values
        [EnableCors("CustomerList")]
        [HttpGet("{page}/{value}")]
        public ActionResult<List<Customer>> Get(int page, string value)
        {
            return _repository.CustomerList(page, value);
        }

        // POST api/values
        [EnableCors("CustomerAdd")]
        [HttpPost("Add")]
        public void Post([FromBody] Customer customer)
        {
            _repository.AddCustomer(customer);
        }

        // PUT api/values/5
        [EnableCors("CustomerCount")]
        [HttpGet("Count/{searchValue}")]
        public int GetCount(string searchValue)
        {
            System.Diagnostics.Debug.WriteLine(searchValue);
            return _repository.CustomerCount(searchValue);
        }
    }
}
