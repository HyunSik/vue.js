﻿using System.Collections.Generic;
using System.Linq;
using Core.Models.Abstract;
using Core.Models.Entities;
using Core.Models.ViewModel;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SalesController : ControllerBase
    {
        private ISalesMasterRepository _smRepository;
        private ISalesDetailRepository _sdRepository;
        private ICustomer2Repository _cRepository;
        private IServiceRepository _sRepository;

        public SalesController(ISalesMasterRepository smRepository, ICustomer2Repository cRepository, ISalesDetailRepository sdRepository, IServiceRepository sRepository)
        {
            _smRepository = smRepository;
            _cRepository = cRepository;
            _sdRepository = sdRepository;
            _sRepository = sRepository;
        }

        // GET: api/Default/5
        [EnableCors("SalesList")]
        [HttpGet("{id}")]
        public SalesListViewModel Get(int id)
        {
            SalesListViewModel model = new SalesListViewModel
            {
                Customer = _cRepository.GetCustomer(id),
                Count = _smRepository.SalesMaster().Where(x => x.CustIdx == id).Count(),
                SalesList = _smRepository.SalesMaster().Where(x => x.CustIdx == id).OrderBy(x => x.SmIdx).ToList(),
                Sum = _smRepository.SalesMaster().Where(x => x.CustIdx == id).Sum(x => x.Card + x.Cash - x.Misu),
                ServiceList = null
            };

            List<List<SalesDetail>> list = new List<List<SalesDetail>>();

            List<SalesDetail> list2 = new List<SalesDetail>();

            foreach (var item in model.SalesList)
            {
                foreach (var item2 in _sdRepository.GetSalesDetail(item.SmIdx))
                {
                    item2.ServiceName = _sRepository.GetServiceName(item2.SvcIdx);
                    list2.Add(item2);
                }
                list.Add(list2);
                list2 = new List<SalesDetail>();
            }

            model.ServiceList = list;

            return model;
        }
    }
}
