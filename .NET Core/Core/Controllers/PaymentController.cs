﻿using System;
using System.Collections.Generic;
using Core.Models.Abstract;
using Core.Models.Entities;
using Core.Models.ViewModel;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Core.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentController : ControllerBase
    {
        private IServiceClassRepository _scRepository;
        private IServiceRepository _sRepository;
        private ISalesMasterRepository _smRepository;
        private ISalesDetailRepository _sdRepository;

        public PaymentController(IServiceClassRepository scRepository, IServiceRepository sRepository, ISalesMasterRepository smRepository, ISalesDetailRepository sdRepository)
        {
            _scRepository = scRepository;
            _sRepository = sRepository;
            _smRepository = smRepository;
            _sdRepository = sdRepository;
        }

        // GET: api/Default
        [EnableCors("ServiceClassList")]
        [HttpGet("ServiceClass")]
        public List<ServiceClass> GetServiceList()
        {
            return _scRepository.GetServiceClass();
        }

        [EnableCors("ServiceList")]
        [HttpGet("Service/{idx}")]
        public List<Service> GetService(int idx)
        {
            return _sRepository.GetServiceList(idx);
        }

        [EnableCors("PaymentEdit")]
        [HttpGet("EditView/{smIdx}")]
        public JObject GetPayment(int smIdx)
        {
            SalesMaster sm = _smRepository.GetSalesMaster(smIdx);
            List<SalesDetail> sd = _sdRepository.GetSalesDetail(sm.SmIdx);

            foreach (var item in sd)
            {
                item.ServiceName = _sRepository.GetServiceName(item.SvcIdx);
            }

            JObject jObject = new JObject();

            JToken smToken = JToken.FromObject(sm);
            JToken sdToken = JToken.FromObject(sd);

            jObject.Add("SalesMaster", smToken);
            jObject.Add("SalesDetail", sdToken);

            return jObject;
        }

        [EnableCors("EditSales")]
        [HttpPut("Edit")]
        public void Put([FromBody]JObject data)
        {
            SalesMaster salesMaster = new SalesMaster
            {
                Card = data["card"].Value<int>(),
                Cash = data["totalPrice"].ToObject<int>() - data["card"].Value<int>() - data["misu"].ToObject<int>(),                       // value랑 차이가 뭐징, 이거는 JToken에서.. Value는 Linq인 듯?
                Memo = data["memo"].ToObject<string>(),
                Misu = data["misu"].ToObject<int>(),
                CustIdx = data["custIdx"].ToObject<int>(),
                Date = data["date"].ToObject<DateTime>(),
                TotalPrice = data["totalPrice"].ToObject<int>(),
                SmIdx = data["smIdx"].ToObject<int>(),
            };
            _smRepository.EditSalesmaster(salesMaster);

            _sdRepository.DeleteSalesDetail(salesMaster.SmIdx);

            foreach (var item in data["salesList"].ToObject<List<JObject>>())
            {
                _sdRepository.AddSalesDetail(new SalesDetail
                {
                    SmIdx = salesMaster.SmIdx,
                    Amount = item["qty"].ToObject<int>(),
                    Price = item["svcAmount"].ToObject<int>(),
                    SvcIdx = item["svcIdx"].ToObject<int>()
                });

            }

        }

        [EnableCors("AddSales")]
        [HttpPost]
        public void Post([FromBody]JObject data) // 굳이 ViewModel 만들지 않고 처리
        {
            //data[""].ToObject<type>()
            SalesMaster salesMaster = new SalesMaster {
                Card = data["card"].Value<int>(),
                Cash = data["cash"].ToObject<int>(), // value랑 차이가 뭐징, 이거는 JToken에서.. Value는 Linq인 듯?
                Memo = data["memo"].ToObject<string>(),
                Misu = data["misu"].ToObject<int>(),
                CustIdx = data["custIdx"].ToObject<int>(),
                Date = data["date"].ToObject<DateTime>(),
                TotalPrice = data["totalPrice"].ToObject<int>()
            };
            _smRepository.AddSalesMaster(salesMaster);

            int smIdx = _smRepository.GetSalesMasterIdx();

            foreach (var item in data["salesList"].ToObject<List<JObject>>())
            {
                _sdRepository.AddSalesDetail(new SalesDetail {
                    SmIdx = smIdx,
                    Amount = item["qty"].ToObject<int>(),
                    Price = item["svcAmount"].ToObject<int>(),
                    SvcIdx = item["svcIdx"].ToObject<int>()
                });
            }
        }
    }
}
