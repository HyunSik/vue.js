﻿using Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.ViewModel
{
    public class PaymentViewModel
    {
        public int Cash { get; set; }
        public int Card { get; set; }
        public int Misu { get; set; }
    }
}
