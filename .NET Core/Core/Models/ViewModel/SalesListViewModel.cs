﻿using Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.ViewModel
{
    public class SalesListViewModel
    {
        public List<SalesMaster> SalesList { get; set; } // 판매 리스트
        public int Sum { get; set; } // 매출 총액
        public int Count { get; set; } // 판매 총 수
        public Customer2 Customer { get; set; } // 고객 정보
        public List<List<SalesDetail>> ServiceList { get; set; } // 해당 판매정보의 서비스 코드 리스트
    }
}
