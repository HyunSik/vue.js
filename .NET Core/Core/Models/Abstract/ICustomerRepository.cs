﻿using Core.Models.Entities;
using System.Collections.Generic;

namespace Core.Models.Abstract
{
    public interface ICustomerRepository
    {
        List<Customer> CustomerList(int page, string searchValue); // 고객 리스트
        void AddCustomer(Customer customer); // 고객 등록
        int CustomerCount(string searchValue);
    }
}
