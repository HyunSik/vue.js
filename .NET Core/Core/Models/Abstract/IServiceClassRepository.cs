﻿using Core.Models.Entities;
using System.Collections.Generic;

namespace Core.Models.Abstract
{
    public interface IServiceClassRepository
    {
        List<ServiceClass> GetServiceClass();
    }
}
