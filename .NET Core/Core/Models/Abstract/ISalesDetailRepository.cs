﻿using Core.Models.Entities;
using System.Collections.Generic;

namespace Core.Models.Abstract
{
    public interface ISalesDetailRepository
    {
        List<SalesDetail> GetSalesDetail(int smIdx);
        void AddSalesDetail(SalesDetail salesDetail);
        void DeleteSalesDetail(int smIdx);
    }
}
