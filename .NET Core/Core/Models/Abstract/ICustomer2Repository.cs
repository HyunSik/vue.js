﻿using Core.Models.Entities;
using System.Collections.Generic;

namespace Core.Models.Abstract
{
    public interface ICustomer2Repository
    {
        Customer2 GetCustomer(int custIdx);
    }
}
