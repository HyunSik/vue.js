﻿using Core.Models.Entities;
using System.Collections.Generic;

namespace Core.Models.Abstract
{
    public interface IServiceRepository
    {
        string GetServiceName(int svcIdx);
        List<Service> GetServiceList(int sclassIdx);
    }
}
