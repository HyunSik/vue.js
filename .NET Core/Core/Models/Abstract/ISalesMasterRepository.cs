﻿using Core.Models.Entities;
using System.Collections.Generic;

namespace Core.Models.Abstract
{
    public interface ISalesMasterRepository
    {
        IEnumerable<SalesMaster> SalesMaster(); // 판매내역 리스트
        void AddSalesMaster(SalesMaster salesMaster);
        int GetSalesMasterIdx();
        SalesMaster GetSalesMaster(int smIdx);
        void EditSalesmaster(SalesMaster salesMaster);
    }
}
