﻿using Core.Models.Abstract;
using Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.Concrete
{
    public class Customer2Repository : ICustomer2Repository
    {
        private Customer2Context _context;

        public Customer2Repository(Customer2Context context)
        {
            _context = context;
        }

        // 고객 정보 가져오기
        public Customer2 GetCustomer(int custIdx)
        {
            return _context.Customer2.SingleOrDefault(c => c.CustIdx == custIdx);
        }
    }
}
