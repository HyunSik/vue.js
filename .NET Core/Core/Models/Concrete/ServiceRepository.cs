﻿using Core.Models.Abstract;
using Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.Concrete
{
    public class ServiceRepository : IServiceRepository
    {
        private ServiceContext _context;

        public ServiceRepository(ServiceContext context)
        {
            _context = context;
        }

        // 서비스 네임 리턴
        public string GetServiceName(int svcIdx)
        {
            return _context.Service.FirstOrDefault(x=> x.SvcIdx == svcIdx).SvcName;
        }

        // 서비스 코드에 따른 서비스 리스트 리턴
        public List<Service> GetServiceList(int sclassIdx)
        {
            return _context.Service.Where(x => x.SclassIdx == sclassIdx).ToList();
        }
    }
}
