﻿using Core.Models.Abstract;
using Core.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.Concrete
{
    public class SalesMasterRepository : ISalesMasterRepository
    {
        private SalesMasterContext _context;

        public SalesMasterRepository(SalesMasterContext context)
        {
            _context = context;
        }

        // 판매내역 리스트
        public IEnumerable<SalesMaster> SalesMaster()
        {
            return _context.SalesMaster.OrderByDescending(x => x.SmIdx);
        }

        public void AddSalesMaster(SalesMaster salesMaster)
        {
            _context.SalesMaster.Add(salesMaster); // dbset.add도 가능
            _context.SaveChanges();
        }

        public void EditSalesmaster(SalesMaster salesMaster)
        {
            // 삭제하고 추가
            //_context.SalesMaster.Remove(_context.SalesMaster.FirstOrDefault(x => x.SmIdx == salesMaster.SmIdx));
            //_context.SalesMaster.Add(salesMaster);
            //_context.SaveChanges();

            // sm에 salesMaster를 추가하니 안된다...
            SalesMaster sm = _context.SalesMaster.FirstOrDefault(x => x.SmIdx == salesMaster.SmIdx);
            sm.TotalPrice = salesMaster.TotalPrice;
            sm.Misu = salesMaster.Misu;
            sm.Memo = salesMaster.Memo;
            sm.Date = salesMaster.Date;
            sm.CustIdx = salesMaster.CustIdx;
            sm.Cash = salesMaster.Cash;
            sm.Card = salesMaster.Card;
            _context.SaveChanges();
        }

        public int GetSalesMasterIdx()
        {
            return _context.SalesMaster.OrderByDescending(x => x.SmIdx).Select(x => x.SmIdx).FirstOrDefault();
        }

        public SalesMaster GetSalesMaster(int smIdx)
        {
            return _context.SalesMaster.Where(x => x.SmIdx == smIdx).FirstOrDefault();
        }
    }
}
