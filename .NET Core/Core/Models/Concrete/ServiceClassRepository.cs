﻿using Core.Models.Abstract;
using Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.Concrete
{
    public class ServiceClassRepository : IServiceClassRepository
    {
        private ServiceClassContext _context;

        public ServiceClassRepository(ServiceClassContext context)
        {
            _context = context;
        }

        public List<ServiceClass> GetServiceClass()
        {
            return _context.ServiceClass.ToList();
        }
    }
}
