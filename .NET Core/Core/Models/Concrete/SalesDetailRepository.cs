﻿using Core.Models.Abstract;
using Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Core.Models.Concrete
{
    public class SalesDetailRepository : ISalesDetailRepository
    {
        private SalesDetailContext _context;

        public SalesDetailRepository(SalesDetailContext context)
        {
            _context = context;
        }

        public List<SalesDetail> GetSalesDetail(int smIdx)
        {
            return _context.SalesDetail.Where(x => x.SmIdx == smIdx).ToList();
        }

        public void AddSalesDetail(SalesDetail salesDetail)
        {
            _context.Add(salesDetail); // _context에 add를 하네...
            _context.SaveChanges();
        }

        public void DeleteSalesDetail(int smIdx)
        {
            _context.RemoveRange(_context.SalesDetail.Where(x => x.SmIdx == smIdx));
            _context.SaveChanges();
        }
    }
}
