﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Core.Models.Abstract;
using Core.Models.Entities;

// Dapper 사용
namespace Core.Models.Concrete
{
    public class CustomerRepository : ICustomerRepository
    {
        private readonly IConfiguration _configuration;
        private IDbConnection db;

        public CustomerRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            string connectionString = _configuration.GetSection("ConnectionString").Value;
            db = new SqlConnection(connectionString);
        }

        // 고객 리스트
        public List<Customer> CustomerList(int page, string searchValue)
        {
            if (searchValue == "true")
                searchValue = "";

            string sql = @"
                Select * From CustInfo Where Hp Like '%" + searchValue + "%' or CustName Like '%" + searchValue + "%' Order by custIdx Desc Offset (" + page + " - 1) * 3 Rows Fetch Next 3 Rows Only";
            return db.Query<Customer>(sql).ToList();
        }

        // 고객 등록
        public void AddCustomer(Customer customer)
        {
            string sql = @"
                Insert CustInfo Values(@CustName, @Hp)";

            db.Execute(sql, new { CustName = customer.CustName, Hp = customer.Hp });
        }

        public int CustomerCount(string searchValue)
        {
            System.Diagnostics.Debug.WriteLine(searchValue);
            if (searchValue == "true")
                searchValue = "";

            string sql = @"
                Select * From CustInfo Where Hp Like '%" + searchValue + "%' or CustName Like '%" + searchValue + "%'";
            return db.Query<Customer>(sql).Count();
        }
    }
}
