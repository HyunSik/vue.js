﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.Entities
{
    [Table("Sclass")]
    public class ServiceClass
    {
        [Key]
        public int SclassIdx { get; set; } // 서비스 분류 번호
        public string SclassName { get; set; } // 서비스 분류명
    }

    public class ServiceClassContext : DbContext
    {
        public ServiceClassContext(DbContextOptions<ServiceClassContext> options) : base(options) { }

        public DbSet<ServiceClass> ServiceClass { get; set; }
    }
}