﻿using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.Entities
{
    [Table("SalesMaster")]
    public class SalesMaster
    {
        [Key]
        public int SmIdx { get; set; } // 판매 마스터 번호
        public DateTime Date { get; set; } // 판매 일자
        public int TotalPrice { get; set; } // 판매 총 금액
        public int Cash { get; set; } // 현금
        public int Card { get; set; } // 카드
        public int Misu { get; set; } // 미수금
        public string Memo { get; set; } // 메모
        public int CustIdx { get; set; } // 고객 번호
    }

    public class SalesMasterContext : DbContext
    {
        public SalesMasterContext(){}

        public SalesMasterContext(DbContextOptions<SalesMasterContext> options) : base(options) { }

        public DbSet<SalesMaster> SalesMaster { get; set; }
    }
}
