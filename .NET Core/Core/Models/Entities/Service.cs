﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.Entities
{
    [Table("SvcInfo")]
    public class Service
    {
        [Key]
        public int SvcIdx { get; set; } // 서비스 코드 번호
        public int SclassIdx { get; set; } // 서비스 분류 번호
        public string SvcName { get; set; } // 서비스 코드명
        public int SvcAmount { get; set; } // 서비스 코드 가격
    }

    public class ServiceContext : DbContext
    {
        public ServiceContext(DbContextOptions<ServiceContext> options) : base(options) { }

        public DbSet<Service> Service { get; set; }
    }
}
