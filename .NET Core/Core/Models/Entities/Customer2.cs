﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.Entities
{
    [Table("CustInfo")]
    public class Customer2
    {
        [Key]
        public int CustIdx { get; set; } // 고객 번호
        public string CustName { get; set; } // 고객명
        public string Hp { get; set; } // 고객 핸드폰 번호
    }

    public class Customer2Context : DbContext
    {
        public Customer2Context(DbContextOptions<Customer2Context> options) : base(options) { }

        public DbSet<Customer2> Customer2 { get; set; }
    }
}
