﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Models.Entities
{
    [Table("SalesDetail")]
    public class SalesDetail
    {
        [Key, Column(Order = 0)]
        public int SmIdx { get; set; } // 판매 마스터 번호
        [Key, Column(Order = 1)]
        public int SvcIdx { get; set; } // 서비스 코드 번호
        public int Price { get; set; } // 가격
        public int Amount { get; set; } // 수량
        [NotMapped]
        public string ServiceName { get; set; } // 서비스 명 
    }

    public class SalesDetailContext : DbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<SalesDetail>()
                .HasKey(x => new { x.SmIdx, x.SvcIdx });
        }

        public SalesDetailContext(DbContextOptions<SalesDetailContext> options) : base(options){}

        public DbSet<SalesDetail> SalesDetail { get; set; }        
    }
}