﻿namespace Core.Models.Entities
{
    public class Customer
    {
        public int CustIdx { get; set; } // 고객 번호
        public string CustName { get; set; } // 고객명
        public string Hp { get; set; } // 고객 핸드폰 번호
    }
}
