﻿using Core.Models.Abstract;
using Core.Models.Concrete;
using Core.Models.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Core
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // 하나하나씩 다 등록하기에는 ㅠ.ㅠ
            services.AddCors(o => o.AddPolicy("SalesList", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddCors(o => o.AddPolicy("CustomerList", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddCors(o => o.AddPolicy("CustomerAdd", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddCors(o => o.AddPolicy("ServiceClassList", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddCors(o => o.AddPolicy("ServiceList", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddCors(o => o.AddPolicy("AddSales", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddCors(o => o.AddPolicy("PaymentEdit", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddCors(o => o.AddPolicy("EditSales", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));
            services.AddCors(o => o.AddPolicy("CustomerCount", builder =>
            {
                builder.AllowAnyOrigin()
                       .AllowAnyMethod()
                       .AllowAnyHeader();
            }));

            services.AddDbContext<SalesDetailContext>(
                options => options.UseSqlServer(Configuration["ConnectionString"]));
            services.AddDbContext<SalesMasterContext>(
                options => options.UseSqlServer(Configuration["ConnectionString"]));
            services.AddDbContext<ServiceClassContext>(
                options => options.UseSqlServer(Configuration["ConnectionString"]));
            services.AddDbContext<ServiceContext>(
                options => options.UseSqlServer(Configuration["ConnectionString"]));
            services.AddDbContext<Customer2Context>(
                options => options.UseSqlServer(Configuration["ConnectionString"]));

            services.AddSingleton<IConfiguration>(Configuration); // 이거 없어도 실행되네

            services.AddTransient<ICustomerRepository, CustomerRepository>();
            services.AddTransient<ICustomer2Repository, Customer2Repository>();
            services.AddTransient<ISalesMasterRepository, SalesMasterRepository>();
            services.AddTransient<ISalesDetailRepository, SalesDetailRepository>();
            services.AddTransient<IServiceRepository, ServiceRepository>();
            services.AddTransient<IServiceClassRepository, ServiceClassRepository>();


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
